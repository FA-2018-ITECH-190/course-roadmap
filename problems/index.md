---
layout: default
---

# Problem Solving Strategies

---

Throughout the course, we are going to learn a variety of different computational problem solving strategies. Practice for these strategies and techniques will be reinforced through a series of daily problems.

* [Introduction]({{ site.basurl }}{% link problems/prob-1.html %})
* [Draw a Diagram]({{ site.basurl }}{% link problems/prob-2.html %})
* [Make a Systematic List]({{ site.basurl }}{% link problems/prob-3.html %})
