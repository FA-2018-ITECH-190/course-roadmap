---
layout: default
title: Chapter 6 - Functions
--- 

# Defining Functions

## What's a function?

* So far, with few exceptions, we have only been writing programs with one function.
* `main()`
* We have also used pre-written functions that are built in `print()` and `abs()`
* and methods from other libraries, like the graphics library `myPoint.getX()`
* Consider the futval program. It works, but stylistically, it leaves a lot to be desired.
* Consider that it has two places where it draws bars. This repetition of code is something to be avoided.
* Breaking up code like this into functions makes it easier to maintain and update.

## Functions informally

* A function is a *subprogram* or *Subroutine*. It's a miniature program that is run within your main program.
* Once a function is defined, it can be invoked at any time by just *calling* its name.
* The place where it is defined is called a *function definition*. When it is used, we say it is *called* or *invoked*.
* Consider the *happy birthday* example.
* Think of ways to refine it.
* Now `happy.py`

## Future Value with a Function

* Let's look at fuval again. Let's see where code is repeated and think of ways to refine this into a function.
* Let's define a `drawBar()` function.
    * Function definition syntax: 
        ```python
        def <name>(<formal-parameters>):
            <body>
        ```
    * Function calling syntax:
        ```python
        <name>(<actual-parameters>)
        ```
    * Why does the function need the window as a parameter?
* Why python comes to a function it does the following
    1. The calling program suspends execution at the point of the call.
    1. The formal parameters of the function get assigned the values supplied by the actual parameters in the call.
    1. The body of the function is executed.
    1. Control returns to the point just after where the function was called.
* Go back to `happy birthday`
* What do we do when there are multiple parameters?
    * They are defined by *position*
* So let's finally look back at the `drawBar()` function.
    * How do the parameters work?
    * As if there are three assignment statements

## Functions that return values

* Algebraic functions: $$ f(x) = x^2 $$
    * What is $$ f(5) $$ ?
* How about the Pythagorean Theorem?
    * $$ h(x,y) = \sqrt{x^2 + y^2} $$
* //TODO
