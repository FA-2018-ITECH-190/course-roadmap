---
layout: default
title: Chapter 2
---

# Writing Simple Programs 

## Software Development Process

* How do you write programs?
* Waterfall Model
    * Analyze the Problem
    * Determine Specifications
    * Create a Design
    * Implement the Design
    * Test/Debug the program
    * Maintain the Program
* Example: Temperature Converter
    * Analysis: We are getting temperatures in C and only really understand F
    * Specification:
        * Input? Temp in C
        * Output? Temp in F
        * What is the relationship? $$ \frac{212-32}{100-0} = \frac{180}{100} = \frac{9}{5} $$
        * Formula: $$ F = \frac{9}{5}C + 32 $$
        * Note this is just the simplest specification
    * Design:
        * *Input, Process, Output (IPO)*
        * Let's talk about psuedocode
    * Implement:

        ```python
        # convert.py
        # Converts Celsius to Fahrenheit

        def main():
            celsius = input("What is the temp in C? ")
            fahr = 9/5 * celsius + 32
            print("The temperature is", fahr, "degrees Fahrenheit")

        main()
        ```

    * Test
        * Make sure to test the program thoroughly.
    * Maintain?

## Elements of Programs

* Names
    * Everything has names: Modules, functions, variables. *Identifiers*
    * Python identifier rules:
        * must begin with a letter or an underscore "\_"
        * It must be followed by any combination of letters, digits or underscores
        * *Cannot contain spaces*
    * Legal Identifiers
        * `x`
        * `celsius`
        * `spam`
        * `spam2`
        * `SpamAndEggs`
        * `Spam_and_Eggs`
        * `_mySpam`
    * Identifiers are case sensitive so the following are different:
        * spam
        * sPam
        * SPAM
    * [Reserved words](https://www.programiz.com/python-programming/keywords-identifier)
    * Built-in functions
        * Check page 503 of the text
* Expressions
    * Two primary kinds of data
        * Numbers
        * Text
    * Literals
        * Simplest kind of expression
    * Strings
    * Converting expressions into an actual data value is called *evaluation*.
        * Consider simple expression evaluation
        * Basic mathematical evaluation
        * Notice how Python formats strings
    * We'll talk about data types a little later
    * An **identifier** can also be an expression.

      ```
      >>> x = 5
      >>> x
      5
      ```

    * What happens if we try to use an identifier with no value?
* Mathematical expression
* All basic **operators** are included. Adding, subtracting, multiplying, dividing, and exponentiation

    | operation      | operator |
    |----------------+----------|
    | Addition       | +        |
    | Subtraction    | -        |
    | Multiplication | *        |
    | Division       | /        |
    | Exponents      | **       |
    | Grouping       | ()       |

* Consider a few expressions

  ```python
  3.9 * x * (1 - x)
  9/5 * celsius + 32
  ```

* Spaces are irrelevant. However, they do make things look nice. It's generally considered good style to put spaces on both sides of each operator.
* Python obeys the standard PEMDAS order of operation, except that only parentheses may be used for grouping (no brackets)
* From the text: Operators also work on strings (i.e. concatenation)

## Output Statements

* A discussion of meta-notation (Grammar!)
    * Syntax (form)
    * Semantics (meaning)
* Let's look at the `print` statement

  ```python
  print(<expr>, <expr>, ..., <expr>)
  print()
  ```

* These are the two forms that the `print` statment can take.
    * Look at what a template means as an expression of how a grammar works.
        ```python
        print(3+4)
        print(3,4,3+4)
        print()
        print("The answer is", 3 + 4)
        ```
    * Notice:
        * they print on separate lines
        * empty parens makes a blank line `"\n"`
        * you can override the `end="\n"` parameter

## Assignment Statements

* Basic form: `<variable> = <expr>`
    * Think about syntax vs semantics
    * `x = 3.9 * x * (1 - x)`
    * `fahrenheit = 9 / 5 * celsius + 32`
    * `x = 5`
* Variables can be reassigned as often as you like.
    * Retains value of last assignment.
    * Try this.
    * You can even re-use the current value of a variable to re-assign it. (for example, incrementing)
    * A variable is a named storage location in computer memory that you can put a value in.
    * page 38 - practical mental concept vs what's actually happening.
    * Though memory is never overwritten during an assignment, you don't run out because of garbage collection.
* Assigning Input
    * `<variable> = input(<prompt>)`
    * `<variable> = eval(input(<prompt>))`
    * Let's talk about the `eval()` function
        * Never, ever use `eval()` in a pdoruction environment.
* Simultaneous Assignment
    * `<var1>, <var2>,..., <varn> = <expr1>, <expr2>,..., <exprn>`
    * sum, diff = x+y, x-y
    * Swapping values.
        * As an alternative, you can use a temporary value.

    ```python
    def main():
        print("This program computes the average of two exam scores.")

        score1, score2 = eval(input("Enter two scores separated by a comma: "))
        average = (score1 + score2) / 2

        print("The average score is: ", average)
    main()
    ```
    * This is better than the alternative of using two input statements.

## Definite Loops

* Look back at the chaos program

    ```python
    for <var> in <sequence>:
        <body>
    ```

* This is a counted loop. Usually called a "for loop"
    * `for` is a keyword
    * the variable is called the *loop index*
    * The sequence is often a list of values.
    * See examples on page 44
* Then the `range()` function
    ```python
    for i in range(10):
        print(i)
    ```
* `range(10)` becomes an explicit list.
* Talk about 0 indexing
* This is called a *counted loop*. It is by far the most common type of loop. Not only that it's one of the single most important semantic constructs in all of programming. So memorize this syntax. Practice it. Make it part of who you are. Like, as a person.

## Example Program: Future Value

* [Link](python/Code/chapter02/futval.py)
* We want to develop a program to determine future value of an investment.
* **Analysis**
    * Given the principal and interest rate, what will an investment be 10 years in the future?
* **Specification**
    * What are the inputs?
    * How will the inputs be formatted? (How to represent percentages)
    * **Relationship**: $$ principal(1+apr) $$
* **Design an algorithm**
    * Use pseudocode
        * Print an introduction
        * Input the amount of the principal
        * Input the APR
        * Repeat 10 times:
            * prin = prin * (1 + apr)
        * Output final value of prin
    * Note: There is a formula to do this all in one step. However, we are demonstrating loops. Plus, this is actually easier to understand.
* **Implementation**
    * Translate each line into some Python code.
    * Demonstrate utility of extra whitespace
* **Testing and Debugging** 
    * Try a few examples
