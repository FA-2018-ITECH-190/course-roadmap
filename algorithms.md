---
layout: default
title: Algorithms
---

# Algorithms Demos

* [Sort 1](first_algorithm.html)
* [Sort 2](second_algorithm.html)
* [Binary Search](search.html)
* [Newton's Method](newton.html)
* [Hungarian Folk Dance](https://flowingdata.com/2011/04/14/sorting-algorithms-demonstrated-with-hungarian-folk-dance/)
* [Sorting Algorithms](https://www.toptal.com/developers/sorting-algorithms)
* [More Sorting Algorithms](https://visualgo.net/en/sorting?slide=1)
