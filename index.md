---
layout: default
---

<a href="https://gitlab.com/FA-2018-ITECH-190/course-roadmap">
    <span style="font-family: tahoma; font-size: 20px; position:fixed; top:50px; right:-45px; display:block; -webkit-transform: rotate(45deg); -moz-transform: rotate(45deg); background-color:red; color:white; padding: 4px 30px 4px 30px; z-index:99">Fork Me On GitLab</span>
</a>


# ITECH 190 Roadmap

[Example Code and Solutions](python/index.html) <br>
[Problem Solving](problems/index.html) <br>
[Daily problems](daily_problems.html)

| Chapter   | Content                                                    | Lab                                              |
| ------    | ------------------------------------------------------     | ------------------------------------------------ |
| 1         | [Chapter 1](chapter1.html) - Computers and Programs        | [Lab 0](https://classroom.github.com/a/guEfCbaj) |
| 2         | [Chapter 2](chapter2.html) - Writing Simple Programs       | [Lab 1](https://classroom.github.com/a/sLDLqNEc) |
| 3         | [Chapter 3](chapter3.html) - Computation with Numbers      | [Lab 2](https://classroom.github.com/a/VZ_8-97E) |
| 4         | [Chapter 4](chapter4.html) - Graphics                      | [Lab 3](https://classroom.github.com/a/rB_uQDkE) |
| 5         | [Chapter 5](chapter5.html) - Sequences                     | TBD                                              |
| Interlude | [Binary](binary.html) - Binary Representation              | [Lab 4](https://classroom.github.com/a/Okr-8zmX) |
| Interlude | [Color Representation](color.html) - Binary Representation | TBD                                              |
| 6         | [Chapter 6](chapter6.html) - Functions                     | TBD                                              |
| 7         | [Chapter 7](chapter7.html) - Control Flow                  | TBD                                              |
| Epilogue  | [Algorithms](algorithms.html)                              | [Lab 7](https://classroom.github.com/a/K0Cf1bTL) |
