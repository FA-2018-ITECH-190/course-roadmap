---
layout: default
title: Chapter 5 - Sequences
--- 

# Sequences: Strings, Lists, Files

## The String

* Text processing is one of the biggest aspects of CS.
* *string* data type.
* you can use quotation marks ('' or "") to enclose character sequences to create a string literal.
    ```python
    str1 = "hello"
    str2 = 'Eric'
    print(str1, str2)
    type(str1)
    type(str2)
    ```
* Remember with the `input()` function, you don't have to cast anything. It outputs a string!
* Let's look at some string operations
* A string is a sequence of characters

    <img src="images/string.jpg" width="70%" alt="objects"/>

* Try this:

    ```python
    greet = "Hello Bob"
    greet[0]
    print(greet[0], greet[2], greet[4])
    x = 8
    print(greet[x-2])
    ```

* Remember 0-indexing
* Negative indexes count from the right end of the string

    ```python
    greet[-1]
    greet[-3]
    ```

* Getting the last character of a string is a common operation.
* Slicing has the syntax: `<string>[<start>:<end>]`
    * This gets all the characters starting at `<start>` and up to *but not including* `<end>`
* Try a few examples
    * Try leaving out one or the other number
*  `+` and `*` operators are also supported
* Basic string operations:

| Operator                | Meaning                      |
| :--------:              | :-:                          |
| +                       | Concatenation                |
| *                       | Repetition                   |
| `<string>[]`            | indexing                     |
| `<string>[:]`           | slicing                      |
| `len(<string>)`         | length                       |
| `for <var> in <string>` | iteration through characters |

## Simple String Processing

Let's try some things.

* Suppose we wish to generate usernames from someone's first and last name.
    * Schema: first letter of first name, up to seven letters of last name.
* Algorithm:
    * print intro
    * get first name
    * get last name
    * concatenate first initial with 7 chars of last name
    * output result
* `username.py`
* Another one: Getting the 3 letter month abbreviation from the number of a month.
    * `months = "JanFebMarAprMayJunJulAugSepOctNovDec"`
    * `monthAbbrev = months[pos:pos+3]`
    * Now, to get the correct numbers, we simply take the number of the month, subtract 1, and multiply by three to get the abbreviation.
    * `month.py`
* Could we accomplish this if we wanted to output the full month name?

## Lists as Sequences

* Strings in python are sequences. So are lists. The same operators all apply.
    ```python
    [1,2] + [3,4]
    [1,2] * 3
    ```
* Unlike strings, lists can be lists of anything. Not just characters
    * `myList = [1,"Spam", 4 "U"]`
* Let's re-think the month program
    * `month2.py`
    * Notice the line break in the list assignment statement
* Features:
    * Lists are indexed from 0
    * $$n$$th item is at $$n-1$$
* How about returning the entire name of the month?
* Important Difference:
    * Lists are *mutable* where strings are not.
    * Items can be changed in place without having to recreate the list.

## String Representation and Message Encoding

### String Representation

* We need to devise a very simple way of encrypting a message. (Technically, what we will be doing is encoding, and not encrypting).
* First attempt: Represent letters with numbers 1-26.
* Without too many extra steps, this is what computers do to represent strings. A letter is converted to a (binary) number. Sequences of binary numbers are stored in computer memory.
* It doesn't matter which number as long as it is consistent (ASCII, UTF-8, etc). We have a few standards now, but can you imagine the early days?
* *ASCII* = American Standard Code for Information Interchange.
* 0-127 are the characters on an American keyboard plus a few others
* A-Z are 65-90. a-z are 97-122.
* Since ASCII is a bit Anglo-Centric, most computer systems support a larger standard character set called Unicode (derived from ASCII).
    * Python supports utf-8
* There are a couple of built-in Python functions for encoding and decoding characters:
    ```python
    ord("a")
    ord("A")
    chr(97)
    chr(90)
    ```
* There is one more piece of the puzzle. For example, the character &#960; is unicode character 960.
* The original ASCII set was limited to 256 characters because that's how many you can store with 8 bits (1 bytes)
* So the most popular standards of today (utf-8) which support up to 100,000 different characters (including emojis) use a variable bit-length character encoding scheme.
* It's still very West-Centric.

### An encoder

Let's write a very simple "encrypter"

```
Get the message to encode
for each character in the message:
    print the letter number of the character
```

* Observe how characters like spaces are encoded

### A Decoder

```
Get the sequence of numbers to decode
for each number in the input:
    conver the number to a unicode character
    Add the character to the end of a message
Print the message
```

Before the loop, you need an accumulator variable `message`.

There are some complications. How do we get a sequence of numbers to decode? We're going to play with string manipulation. Actual algorithm looks more like this:

```
get sequence of numbers as a string, inString
split inString into a sequence of smaller strings
message = ""
for each of the smaller strings:
    parse the string into a number
    append the Unicode char to message
print message
```

### More String methods

* `.capitalize()`
* `.title()`
* `.lower()`
* `.upper()`
* `.replace()`
* `.center()`
* `.count()`
* `.find()`
* `.join()`

## List Methods

* Problem with the "decryption" program.
    * It is inefficient because strings are immutable. The string has to be rebuilt every time.
* Let's look at how we can use a list to fix this issue.
* Start with the idea of an accumulator
    ```python
    squares = []
    for x in range(1,101):
        squares.append(x*x)
    ```
* So instead of recopying the string every time in the decoder: Let's use a list accumulator and the `.join()` function.
* See code.
* Here are other list methods:

    <img src="images/listmethods.jpg" width="70%" alt="objects"/>

## I/O String Manipulation

### Date Conversion

* Imagine we want to convert a date "10/29/2018" to "October 29, 2018".
* Algorithm in textbook.
* See COde

### String Formatting

Look at the change counting program from Chapter 3


