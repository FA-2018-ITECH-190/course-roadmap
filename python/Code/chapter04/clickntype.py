# clickntype.py
# Graphics program illustrating mouse and keypress inputs

from graphics import *

def main():
    win = GraphWin("Click and Type", 400, 400)
    while(True):
        pt = win.getMouse()
        pt.draw(win)
    
main()
