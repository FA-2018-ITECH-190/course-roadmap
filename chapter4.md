---
layout: default
title: Chapter 4
---

# Objects and Graphics

**Important File**: [graphics.py](https://itech190.erickuha.com/python/Code/chapter04/graphics.py)

* So far, we have operated in a primarily imperative mode
* To build complex systems, we must rely on more complex ways of handling data
* Enter Object-Oriented Programming
* We will also look at simple graphics programming in this chapter
* Python's GUI framework is Tkinter. But it's massive and complex. We will use a simplified version of it: graphics.py
* graphics.py is a **wrapper**

## The Object of Objects
* Object-Oriented programming: we view complex systems as collections of simpler objects.
* We use *objects* as a very technical term with a very specific definition
* Loosely, an object is an active data type that combines data and operations. 
    * They *know stuff* and the *do stuff*.
* We can send "messages" to objects to ask them to do one or more of the things that they are programmed to do. (Method calls)
* Simple example: data processing for a college
    * What is a student?
    * What would we need to be able to do with a Student object?
    * Maybe a college course is an object.
    * What might this object need to be able to do? What would it need to know?

## Simple Graphics Programming

* To use graphics.py, you need to move the file to the folder you will be doing your work in.
* There are other ways to gain access to such a library, but this is the simplest.
* This library allows us to experiment with graphics intuitively without having to worry about a lot of extra tedious detail.

```python
>>> import graphics
>>> win = graphics.GraphWin()
>>> win.close()
```

* Notice the dot notation.
* This time the dot is not from some module from a *variable name*.
    * So. `win` is an object of type `GraphWin`. 
    * One of the things it can do is close itself
* Typing `graphics` before every command is going to get annoying:

```python
>>> from graphics import *
>>> win = GraphWin()
```

* Let's draw
* The window is a collection of *pixels* "Picture Elements". 
* We control the color of each pixel.
* The window is 200x200 pixels.
    * There are 40,000 pixels that we can control.
* But instead of setting every pixel, we will rely on *objects* in the graphics library that all know how to draw themselves.
* Simplest object is a `Point`
    * What is a point in geometry?
    * What are coordinates?
* Traditionally: (0,0) is in the top left and grows larger going down and to the right.
* So top left is (0,0) and bottom right is what?
* Let's draw a point:

```python
>>> p = Point(50,60)
>>> p.getX()
>>> p.getY()
>>> win = GraphWin()
>>> p.draw(win)
>>> p2 = Point(140,100)
>>> p2.draw(win)
```

* Aside from points, there are lines, circles, rectangles, ovals, text, etc

```python
>>> win = GraphWin('Shapes')
>>> center = Point(100,100)
>>> circ = Circle(center, 30)
>>> circ.setFill('red')
>>> circ.draw(win)
Circle(Point(100.0, 100.0), 30)
>>> label = Text(center, "Red Circle")
>>> label.draw(win)
Text(Point(100.0, 100.0), 'Red Circle')
>>> rect = Rectangle(Point(30,30),Point(70,70))
>>> rect.draw(win)
Rectangle(Point(30.0, 30.0), Point(70.0, 70.0))
>>> lin = Line(Point(20,30), Point(180, 165))
>>> lin.draw(win)
Line(Point(20.0, 30.0), Point(180.0, 165.0))
>>> oval = Oval(Point(20,150), Point(180, 199))
>>> oval.draw(win)
Oval(Point(20.0, 150.0), Point(180.0, 199.0))
```

## Using Graphical Objects

* Objects combine data with operations.
* So we need to understand how to create objects and how to request operations on them.
* We used several objects in the above session.
* These are examples of *classes*.
    * And object made from a class is called an *instance*.
    * A python class describes the properties an instance of the object will have
* Fido is a dog. He is an *instance* of the dog *class*.
    * As an instance, we expect certain things to be true about Fido. Though it probably differs from other dogs in some requests
    * The same is true of a point or a line or an oval. (coordinates etc)
    * Maybe he's also an instance of a *sub-class* of dog called Shib Inu.
* To create an instance, we call a *constructor*.

    ```
    <class-name>(<param1>, <param2>, ...)
    ```

* Parameters are often required.

    ```python
    p = Point(50,60)
    ```

* This constructor requires two parameters or it will throw an error. These parameters are the x and y coordinates of the point. <br> 
    <img src="images/point.jpg" width="30%" alt="objects" />
* To request an operation, we send a message to the object.
    * These are like functions that live inside the object.
    * We call these *methods*.

    ```python
    <object>.<method-name>(<param1>,<param2>,...)
    ```

    * Some methods do not require parameters: `p.getX()`
        * These are called *accessors* or more-commonly *getters*.
    * Other methods change the *state* of the instance.
        * For example: `p.move(dx,dy)` move the object `dx` units to the right and `dy` units down.
        * These are called mutators
    * Some methods require parameters that are also objects `circ = Circle(Point(100,100), 30)`

        ```python
        circ = Circle(Point(100,100), 30)
        win = GraphWin()
        circ.draw(win)
        ```

    * Let's examine everything that's happening here.

        <img src="images/objects.jpg" width="70%" alt="objects"/>

* Let's look at an interesting example:

    ```python
    leftEye = Circle(Point(80,50),5)
    leftEye.setFill('yellow')
    leftEye.setOutline('red')
    rightEye = leftEye
    rightEye.move(20,0)
    ```

    * What's wrong here?
    * Mention pointers and references
    * Correct way to fix it (using cut and paste)
    * Actual correct way to fix it (using clone)

# Case Study: Graphing Future Value

* Let's make a graph of the output of the `futval.py` program.
* Concrete: Invest $2000 at 10% interest. We want to show the growth of the investment over 10-years.
* Design:

    ```
    Print an introduction
    Get value of prin and apr
    Create GraphWin
    Draw scale labels on left
    Draw bar at position 0 with height of prin
    For years 1 through 10
        Calculate prin = prin * (1 + apr)
        Draw a bar for this year with correct height
    Wait for user to press Enter
    ```

* To do a graphics example, we need to think carefully about a few things?
* How big should the GraphWin be?
    * Let make the GraphWin a little bigger: 320x240
        ```python
        win = GraphWin("Investment Growth Chart", 320, 240)
        ```
* Scale: Let's say the scale is from 0 to 10K. So we need to elaborate on the scale labels step
    * We have 200 pixels vertically to work with. Let's say 100 pixels is $5000. So the labels should be 50 pixels apart. 200 pixels for 0-10,000. So there's 40 pixels of padding to split 
    ```
    Draw Label " 0.0K" at (20,230)
    Draw Label " 2.5K" at (20,180)
    Draw Label " 5.0K" at (20,130)
    Draw Label " 7.5K" at (20,80)
    Draw Label "10.0K" at (20,30)
    ```

* How about the bars themselves?
    * Lower left is easy. Let's say (40,230)
    * Where's top right?
    * We decided that 100 pixels is $5,000, so 100/5000 = .02 pixels per dollar.
    * So a 2000 bar would be `2000(.02) = 40 pixels`. So the y position of the upper right corner will be `230-(prin)(0.02)`.
        * remember that y decreases as you go up
    * How wide?
        * Window is 320 pixels wide, 40 are eaten up by labels. 280 pixels for 11 bars: `280/11 = 25.4545` So we just make 25 pixels.
        * Right edge of our first bar will be `40+25 = 65`.
          ```
          Draw a rectangle from (40,230_ to (65,230 - prin * 0.02)
          ```
    * So major decisions are made. Here are the dimensions:
        <img src="images/dimensions.jpg" width="90%" alt="objects"/>
* Lower-left corner of each bar:
    * `(year)(25) + 40` (40 pixels on the left edge).
    * `y` coordinate is still 230.
* Upper right coordinate:
    * Add 25 (width of the bar) to the `x` coordinate of lower corner.
    * `y` coordinate we have already determined.
* What is `xll`?
* Final pseudo code:
  ```
  print introduction
  Get value of prin and apr from user
  create a  320x240 GraphWin title "Investment Growth Chart"
  Draw Label " 0.0K" at (20,230)
  Draw Label " 2.5K" at (20,180)
  Draw Label " 5.0K" at (20,130)
  Draw Label " 7.5K" at (20,80)
  Draw Label "10.0K" at (20,30)
  Draw a rectangle from (40,230) to (65,230 - prin * 0.02)
  for year running from 1 to 10:
    Calculate prin = prin * (1 + apr)
    Calculate xll = 25 * year + 40
    Draw a rectangle from (xll,230) to (x11+25, 230 - prin * 0.02)
  Wait for user to press enter.
  ```
## Choosing Coordinates

* When programming, you are always trying to *translate* some real-world "problem" into a representation on a computer.
* With the graph program, we are translating a problem into window-coordinates.
* We did all of our pixel calculations by hand. But that's a huge pain.
* It's tedious and error-prone.
    <img src="images/tictactoe.jpg" width="90%" alt="Tic Tac Toe"/>
* We can apply this same principle to the investment program:
    ```python
    # futval_graph2.py

    from graphics import *

    def main():
        # Introduction
        print("This program plots the growth of a 10-year investment.")

        # Get principal and interest rate
        principal = float(input("Enter the initial principal: "))
        apr = float(input("Enter the annualized interest rate: "))

        # Create a graphics window with labels on left edge
        win = GraphWin("Investment Growth Chart", 320, 240)
        win.setBackground("white")
        win.setCoords(-1.75,-200, 11.5, 10400)
        Text(Point(-1, 0), ' 0.0K').draw(win)
        Text(Point(-1, 2500), ' 2.5K').draw(win)
        Text(Point(-1, 5000), ' 5.0K').draw(win)
        Text(Point(-1, 7500), ' 7.5k').draw(win)
        Text(Point(-1, 10000), '10.0K').draw(win)

        # Draw bar for initial principal
        bar = Rectangle(Point(0, 0), Point(1, principal))
        bar.setFill("green")
        bar.setWidth(2)
        bar.draw(win)
        
        # Draw a bar for each subsequent year
        for year in range(1, 11):
            principal = principal * (1 + apr)
            bar = Rectangle(Point(year, 0), Point(year+1, principal))
            bar.setFill("green")
            bar.setWidth(2)
            bar.draw(win)

        input("Press <Enter> to quit.")
        win.close()

    main()
    ```
* Important to choose a coordinate system that is easy to use and program.

## Interactive Graphics

<dl>
    <dt>Widget</dt>
    <dd>A GUI element with a particular set of attributes. Buttons. Text Boxes, etc.</dd>
    <dt>Event</dt>
    <dd>Generated by clicking a button or moving the mouse. A GUI listens for these events and responds to them.</dd>
</dl>

### Getting Mouse Clicks

* Look at the `click.py` program
* getMouse() returns a point. We can use it like any other Point object.
* Look at `triangle.pyw`. Notice the `.pyw` extension.

### Handling Text Input

* `clickntype.py`
* `convert_gui.py`
*
