---
layout: default
title: Chapter 7 - Decision Structures
--- 

# Simple Decisions

* Programs are not always simple step-by-step procedures. Sometimes, you need to alter that *sequential* program flow.
* Programs can handle different situations when they can make decisions on the fly
* Example: C to F
    * What if we want to print a warning if it's very hot or very cold?
    * Two decisions. Let's see the flowchart.

    <img src="images/flow.jpg" width="60%" />

* How does Python do this?
    * See `convert2.py`
* Syntax:
    ```python
    if <condition>:
        <body>
    ```
* The body is just another sequence of statements. Much like a loop or a function. 
    * We use indentation to indicate the code that should run if the `<condition>` is true.

## Forming Simple Conditions

* Let's look at simple relational operators. Syntax: `<expr> <relop> <expr>`.

    <img src="images/flow_example.jpg" width="50%" />

* Here are the relational operators:

| Python   | Mathematics | Meaning                  |
| -----    | -------     | -----                    |
| $$ < $$  | $$ < $$     | Less than                |
| $$ <= $$ | $$ \leq $$  | Less than or equal to    |
| $$ == $$ | $$ = $$     | Equal to                 |
| $$ >= $$ | $$ \geq $$  | Greater than or equal to |
| $$ >  $$ | $$ > $$     | Greater than             |
| $$ != $$ | $$ \neq $$  | Not equal to             |

* Not in particular the $$ == $$ operator.
* Conditions can compare strings or numbers.
    * Eg: $$ 5 > 2 $$ and $$ B > A $$
* **Boolean Expressions**. George Boole. 19th century mathematician. Had a massive impact on computing as it is today.

## Example

```python
main():
    # Some code
    pass

if __name__ == '__main__`:
    main()
```

## Two-Way Decisions

* Quadratic equation solver

    <img src="images/descrim.jpg" width="50%" />

* Try the following examples with each version of the code:
    * 1,2,3
    * 2,4,1
    * 1,2,1
* 
