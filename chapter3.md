---
layout: default
title: Chapter 3
---

# Computing with Numbers

## Numeric Data Types

* Information stored and manipulated by computers is called *data*.
* A program to computer the value of loose change:
    ```python
    def main():
        print("Change Counter")
        print()

        print("Please enter the count of each coin type.")
        quarters = eval(input("Quarters: "))
        dimes = eval(input("Dimes: "))
        nickels = eval(input("Nickels: "))
        pennies = eval(input("Pennies: "))

        total = quarters * .25 + dimes * .1 + nickels * .05 + pennies * .01
        print()
        print("The total value of your change is", total)
    ```
* What does output look like?
    ```
    Change Counter

    Please enter the count of each coin type.
    Quarters: 5
    Dimes: 3
    Nickels: 4
    Pennies: 6

    The total value of your change is 1.81

    ```
* Two kinds of numbers. Two different *data types*.
    * Whole numbers
    * decimal numbers
* integeers (ints) floating-point (floats)
    * Both can have positive and negative values.
* A literal with no decimal point is an int. If it has a decimal point (even if the fractional part is a 0) is a float.
* Look at the `type()` function.
* Why do we have two different data types for numbers?
    * ints are for things that we can count. This tells the program that there *can't* be a fractional part.
    * Certain operations are more efficient for ints than floats. (very tiny differences)
    * Also, no matter what, floats are approximations. There is a limit to floating-point precision.
    * If you can use an int: do it.
* Built-in Numerica operations

    |----------+------------------|
    | Operator | Operation        |
    |----------+------------------|
    | +        | addition         |
    |----------+------------------|
    | -        | subtraction      |
    |----------+------------------|
    | *        | multiplication   |
    |----------+------------------|
    | /        | float division   |
    |----------+------------------|
    | **       | exponentiation   |
    |----------+------------------|
    | `abs()`  | absolute value   |
    |----------+------------------|
    | //       | integer division |
    |----------+------------------|
    | %        | remainder        |
    |----------+------------------|

* Operations on floats produce floats, and ops on ints produce ints
* In practice, we just let Python do most of the work of determining which types are being used and outputted.
* Division is different
    * Let's try out / and //
    * / always returns a float (even if the division is even)
    * What about `10 // 3`
    * "gozinta" 3 goes into 10 3 times.
    * Remainder/modulo operator
    * The problem with earlier versions of python was that they treated / differently depending on the operands which created problems.

## Type conversions

* Consider `x = 5.0 * 2`
* Python does on-the-fly type conversion. Going to the most general type (float).
* But you can do type conversions as well. (Explicit type conversion)
* Look at `int()` and `float()` functions.
    * Remember, `int()` *truncates*. It doesn't *round*.
* Look at the `round()` function: `round(pi,3)`
* Notice also, that these functions convert string values to ints and floats. A secure alternative to `eval()`
* Look at the code in `change2.py`
* note on simultaneous assignment (which `eval()` handles well).

## Math Library

* Talk about the *math* library.
* What is a *library*?
* look at `quadratic.py`
* Notice the import statement and the use of the `math.sqrt`
* The program works whenever there are real solutions
    * It throws errors whenever the equation has imaginary roots.
    * There are ways to fix this problem, but we will get to them later. We assume good input for our programs for now.
* There is a way to do the quadratic formula without the `sqrt` function using just exponentiation. How?

## Accumulating Results: Factorials

* How many different ways can you order 6 objects?
* Let's write a program that computes factorials?
* Design:
    * Inputs? Computation? Output?
* Second part is the hard part.
* So how do we actually compute a factorial? What's the first step in getting fact(6)?
* Repeated multiplications. We keep track of a running product.
* Concept: *accumulator*.
* Here's the pattern:
    ```
    Initialize an accumulator variable
    Loop until the result is reached
        update the value of the accumulator
    ```
* So let's translate this pattern into python to find the fact(6)
    ```python
    fact = 1
    for factor in [6,5,4,3,2]:
        fact = fact * factor
    ```
* But how about `fact(n)`?
* How does the `range(n)` function work?
* Let's look at the `factorial.py` program
* What are the limitations of this program? (inherent to factorial)

## Limitations of Computer Arithmetic

* Factorials are pretty messed up.
* Using our factorial program, what is the factorial of 100?
* Look at how fast the function grows
* Many programming languages would have a lot of trouble even storing the numbers that this function generates.
