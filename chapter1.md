---
layout: default
title: Chapter 1
---

# Computers and Programs

## The Universal Machine

* What do we use a computer to do?
* So what is a computer that it can do all these things?
    * "A machine that stores and manipulates information under the control of a changeable program."
* How does this differ from a calculator or a gas pump?
* Changeable program.
* SO what's a program?
* "A detailed, step-by-step set of instructions telling a computer exactly what to do."
* Changing the program changes the set of actions that the computer executes.
* The main tenet of computer science is that any general purpose computer *should* be able to do anything that any other general purpose computer can do.

## Program Power

* Software rules Hardware
* Without software telling the hardware what to do, it's just a paperweight.
* Creating software is called *programming*
    * Must be able to see the big picture and small detail simultaneously.
* Virtually anyone can *learn* how to be a programmer.
* Learning to program is useful for anyone who wants to feel more in-control of a computer

## Computer Science

* What is CS?
* Dykstra: Computers are to CS what telescopes are to astronomy.
    * Computers are an important tool for CS, but they are *not*, in themselves, the focus of the study of CS.
* If a computer can carry out any process that we can describe: 
    * What processes can we describe?
    * What can be computed?
* Three main techniques
    * Design
    * Analysis
    * Experimentation
* Algorithm
    * Recipe
* Problem: We can only say that something is computable if we write a program for it and it works.
    * What if we fail to find an algorithm?
    * Thus need for analysis
* Analysis: Examine algorithms and problems mathematically.
    * Some simple problems are not solvable by *any* algorithm.
    * Some are *intractable*
    * Some are too complex or ill-defined for analysis, and thus we use experimentation
* Experiment: Implement a system and observe how it behaves.
    * Testing is crucial to any created system
* Practically: Computer scientists are all over the place doing many, many different things.

## Hardware Basics

* Knowing the basics of computer hardware is helpful.
* Von Neumann Machine
    * Input Devices
    * CPU
    * Main Memory
    * Output Devices
    * Secondary Memory
* Go over what each element does
* Fetch-Execute Cycle
    * Fetch - Decode - Execute
    * Billions of times per second

## Programming Languages

* We can't just talk to a computer to tell it what to do. (Unsolved Problem)
* Also, human languages are ambiguous and imprecise.
    * I saw the man in the park with the telescope.
* So programmers use unambiguous notation called *Programming Languages*
* Syntax - Precise form of language
* Semantics - Precise meaning of language
* Languages I've used
    * Java, Python, C/C++, C#, Lisp, Scheme, Standard ML, PHP, Prolog, JavaScript
* There are many, many more.
* These are all called *high-level* leanguages.
    * Designed to be human readable and writable
* Machine Language
    * Binary coding
    * See page 8 for example and charts
* Compiled and interpreted languages
    * Draw charts
* Highlight differences between compiled and interpreted languages.
    * Compiled - faster
    * Interpreted - More portable/flexible

## Python

* The Python interpreter
* The typical python installation comes with a *shell*.
    * IDLE
* Launch IDLE

```python
print("Hello, World!")

print(2 + 3)

print("2 + 3 =", 2 + 3)
```
* Functions

```python
def hello():
    print("Hello")
    print("No one expects the Spanish Inquisition!")
```

* Invoke the function with `hello()`
* Parameters

```python
def greet(person):
    print("Hello," person)
    print("How are you?")
```

* The `print()` function is built in 
    * You can even use it without parameters
    * Reference a function in the shell without parentheses
* A program has these definitions in a file: *module* or *script*
    * A module is just a text file with Python code
    * You can use any text editor (even a word processor)
    * *Integrated Development Environment*
        * Automatic indenting
        * Code highlighting
        * Interactive development
* File `chaos.py`
    * Note on the `eval()` function
    * **Type this out by hand!**
    * Try the file with different inputs.
    * Run the file and then re-run the function
    * Note on the `__pychache__` folder and `.pyc` file extension
* Python uses a hybrid compile/interpret process: bytecode

## Inside the program

* Go through chaos.py line by line.
    * Comments
    * `print()`
    * Assignment and `input()`
        * Again, `eval()`
    * loop
    * loop body, *assignment* statement and what it means.

## Chaos Example

* The program runs a special mathematical function of the form `k(x)(1-x)`, where `k` is a constant.
    * It's a logistic function
    * Models unstable electronic circuit and population variation.
    * This function can produce chaos.
    * Must be very careful of initial conditions (butterfly effect)
    * Why can't we model weather more than a few days in advance?
* A program's result is only as good as the model that it is constructed from.
    * That is, a bug in the program can lead to errors in the result, but even if the program is correctly written, bad input can lead to errors in the result.

